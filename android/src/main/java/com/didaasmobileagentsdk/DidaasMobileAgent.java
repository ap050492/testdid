package com.didaasmobileagentsdk;

import android.util.Log;

import com.facebook.react.bridge.Promise;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.bridge.WritableArray;
import com.facebook.react.bridge.WritableNativeArray;
import com.google.gson.Gson;

import org.hyperledger.indy.sdk.IndyException;
import org.hyperledger.indy.sdk.did.Did;
import org.hyperledger.indy.sdk.did.DidResults;
import org.hyperledger.indy.sdk.non_secrets.WalletRecord;
import org.hyperledger.indy.sdk.wallet.Wallet;

import java.util.concurrent.ExecutionException;

import javax.annotation.Nonnull;

// import androidx.annotation.RequiresApi;

public class DidaasMobileAgent extends ReactContextBaseJavaModule {

    private final ReactApplicationContext mReactContext;
    private String ErrorCode = "ERROR";
    public DidaasMobileAgent(@Nonnull ReactApplicationContext reactContext) {
        super(reactContext);
        mReactContext = reactContext;
    }

    @Nonnull
    @Override
    public String getName() {
        return "DidaasMobileAgent";
    }

    @ReactMethod
    public void createWallet(String configJson, String credentialsJson, Promise promise) {
        try {
            Wallet.createWallet(configJson, credentialsJson).get();
            promise.resolve(null);
        } catch (Exception e) {
            IndySdkRejectResponse rejectResponse = new IndySdkRejectResponse(e);
            promise.reject(rejectResponse.getCode(), rejectResponse.toJson(), e);
        }
    }
    
    @ReactMethod
    public Wallet openWallet(String configJson, String credentialsJson) {
        try {
            Wallet wallet = Wallet.openWallet(configJson, credentialsJson).get();
            return wallet;
        } catch (Exception e) {
            IndySdkRejectResponse rejectResponse = new IndySdkRejectResponse(e);
            Log.d("Error Wallet Handle", rejectResponse.toJson());
            // promise.reject(rejectResponse.getCode(), rejectResponse.toJson(), e);
            return null;
        }
    }

    @ReactMethod
    public void createAndStoreMyDids(String configJson, String credentialsJson, String didJson, Promise promise) {

        try {
            Wallet wallet = openWallet(configJson, credentialsJson);
            if(wallet == null) {
                wallet.closeWallet().get();
                promise.reject("401","Unauthorized access");
            } else {
                DidResults.CreateAndStoreMyDidResult createMyDidResult = Did.createAndStoreMyDid(wallet, didJson).get();
                String myDid = createMyDidResult.getDid();
                String myVerkey = createMyDidResult.getVerkey();
                WritableArray response = new WritableNativeArray();
                response.pushString(myDid);
                response.pushString(myVerkey);
                wallet.closeWallet().get();
                promise.resolve(response);
            }
        } catch (Exception e) {
            IndySdkRejectResponse rejectResponse = new IndySdkRejectResponse(e);
            promise.reject(rejectResponse.getCode(), rejectResponse.toJson(), e);
        }
    }


    // ********* non_secrets ********* //
    @ReactMethod
    public void addWalletRecord(String configJson, String credentialsJson, String type, String id, String value, Promise promise) {
        Log.d("inside in addWalletRecord", value);
        try {
            Wallet wallet = openWallet(configJson, credentialsJson);
            if(wallet == null) {
                wallet.closeWallet().get();
                promise.reject("401","Unauthorized access");
            } else {
                WalletRecord.add(wallet, "cloud_agent", "id", "value", null).get();
                wallet.closeWallet().get();
                promise.resolve(null);
            }
        } catch (Exception e) {
            IndySdkRejectResponse rejectResponse = new IndySdkRejectResponse(e);
            promise.reject(rejectResponse.getCode(), rejectResponse.toJson(), e);
        }
    }

    class IndySdkRejectResponse {
        private String code;
        private String message;

        private IndySdkRejectResponse(Throwable e) {
            String code = "0";

            if (e instanceof ExecutionException) {
                Throwable cause = e.getCause();
                if (cause instanceof IndyException) {
                    IndyException indyException = (IndyException) cause;
                    code = String.valueOf(indyException.getSdkErrorCode());
                }
            }

            String message = e.getMessage();

            this.code = code;
            this.message = message;
        }

        public String getCode() {
            return code;
        }

        public String getMessage() {
            return message;
        }

        public String toJson() {
            Gson gson = new Gson();
            return gson.toJson(this);
        }
    }

}

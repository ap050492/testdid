const RouterRequest: Function = async (url: string, apiType: string, apiBody: string) => {
    try {
        const response: any = await fetch(url, {
            method: apiType,
            body: apiBody,
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            },
        });
        const responseJson: any = await response.json();
        console.log('Response inside NetworkRequest Helper', responseJson);
        return responseJson;
    } catch (error) {
        console.log('Error inside NetworkRequest helper', error);
        return Promise.reject(error);
    }
};

export default RouterRequest;
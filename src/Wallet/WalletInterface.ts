export interface WalletConfig {
    id: string;
    storage_type?: string[];
    storage_config?: StorageConfig;
}

export interface WalletCredentials {
    key: string;
}

export interface DidJson {
    did?: string;
    seed?: string;
    method_name?: string;
}

interface StorageConfig {
    path?: string
}
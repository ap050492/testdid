import { NativeModules } from "react-native";
import { DBServices } from "../Storage";
import io from "socket.io-client";
import AgentRegistrationService from '../Protocols/AgentRegistration/AgentRegistrationService';
import { WalletConfig, WalletCredentials } from "DidaasMobileAgent/src/Wallet/WalletInterface";
import ConnectionService from '../Protocols/Connections/ConnectionService';

console.log(NativeModules);
const { DidaasMobileAgent }: any = NativeModules;

class Wallet {

  AgentRegistrationService: any;
  socket: any;

  createInvitation = async() => {
    try {
      let response = await ConnectionService.createConnectionWithInvitation({ id: '454545' }, { key: 'dfgh' }, {});
      console.log(response);
      return response;
    }
    catch(error) {
      console.log(error);
      return error;
    }
  }

  agentRegServices = async(url: string, apiType: string, apiBody: string, configJson: WalletConfig, credentialsJson: WalletCredentials) => {
    console.log(url, configJson, credentialsJson, apiBody, apiType);
    try {
      let agentRegResponse: any = await AgentRegistrationService.agentregistration(url, apiType, apiBody, configJson, credentialsJson);
      console.log(agentRegResponse);
      return agentRegResponse;
    }
    catch(error) {
      console.log(error);
      return error;
    }
  }

  createWallet = async(config: Object, credentials: Object) => {
    let response: any = await DidaasMobileAgent.createWallet(
      JSON.stringify(config),
      JSON.stringify(credentials)
    );
    let createDidAndVerKeyResponse: any = await this.createWalletDidStore(config, credentials, {});
    return createDidAndVerKeyResponse;
  };

  createWalletDidStore = async(
    config: any,
    credentials: any,
    didJson: Object
  ) => {
    console.log(config);
    let response: any = await DidaasMobileAgent.createAndStoreMyDids(
      JSON.stringify(config),
      JSON.stringify(credentials),
      JSON.stringify(didJson)
    );
    if (response.length > 0)
      DBServices.storeWallet({
        walletConfig: JSON.stringify(config),
        walletCredentials: JSON.stringify(credentials),
        lable: config.id,
        serviceEndpoint: "",
        publicDid: response[0],
        verKey: response[1]
      });
    return response;
  };

  fetchUserDB = () => {
    let response: Object = DBServices.getWallet();
    console.log(response);
    return response;
  };

  socketInit = (url: string) => {
    this.socket = io(url);
  };

  socketEmit = () => {
    this.socket.emit("events", "Received event at EVENTS channel");
  };

  socketListener = () => {
    this.socket.on("message", (msg: string) => {
      console.log("Message arrived", msg);
    });
  };

}

export default new Wallet();
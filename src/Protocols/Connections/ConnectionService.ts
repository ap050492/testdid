import { NativeModules } from 'react-native';
import { Connection } from './ConnectionInterface';
import { ConnectionState } from './ConnectionState';
import { DBServices } from '../../Storage/index';
import { encodeInvitationToUrl } from '../../Utils/Helpers';
import { createInvitationMessage } from './messages';
import { DidDoc, Service, PublicKey, PublicKeyType, Authentication } from './DidDoc';
import { WalletConfig, WalletCredentials } from '../../Wallet/WalletInterface';

class ConnectionService {

    indySdk: any;

    constructor() {
        this.indySdk = NativeModules.DidaasMobileAgent;
    }

    async createConnectionWithInvitation(configJson: WalletConfig, credentialsJson: WalletCredentials, didJson: Object): Promise<Connection> {        
        const connection = await this.createConnection(configJson,credentialsJson,didJson);
        const invitationDetails = this.createInvitationDetails(connection);
        const invitation = await createInvitationMessage(invitationDetails);
        connection.state = ConnectionState.INVITED;
        connection.invitation = invitation;
        let invitationEncode: any = encodeInvitationToUrl(connection);
        console.warn(JSON.stringify(connection));
        return invitationEncode;
      }

    async createConnection(configJson: WalletConfig, credentialsJson: WalletCredentials, didJson: Object): Promise<Connection> {
        try {
            let createPairwiseDidResponse: any = await this.indySdk.createAndStoreMyDids(JSON.stringify(configJson), JSON.stringify(credentialsJson), JSON.stringify(didJson))
            const publicKey = new PublicKey(`${createPairwiseDidResponse[0]}#1`, PublicKeyType.ED25519_SIG_2018, createPairwiseDidResponse[0], createPairwiseDidResponse[1]);
            const service = new Service(`${createPairwiseDidResponse[0]};indy`, DBServices.getServiceEndpoint(), [createPairwiseDidResponse[1]], DBServices.getServiceEndpoint(), 0, 'IndyAgent');
            const auth = new Authentication(publicKey);
            const did_doc = new DidDoc(createPairwiseDidResponse[0], [auth], [publicKey], [service]);
        
            const connection : Connection = {
              did: createPairwiseDidResponse[0],
              didDoc: did_doc,
              verkey: createPairwiseDidResponse[1],
              state: ConnectionState.INIT,
            };
        
            // this.connections.push(connection);
        
            return connection;
        }
        catch (error) {
            console.log(error);
            return error;
        }
    }

    private createInvitationDetails(connection: Connection) {
        const { didDoc } = connection;
        return {
          label: "Mobile Agent",
          recipientKeys: didDoc.service[0].recipientKeys,
          serviceEndpoint: didDoc.service[0].serviceEndpoint,
          routingKeys: didDoc.service[0].routingKeys,
        };
      }
    

    // async createConnectionWithInvitation(configJson: WalletConfig, credentialsJson: WalletCredentials, didJson: Object): Promise<Connection> {
    //     try {
    //         let createPairwiseDidResponse: any = await this.indySdk.createAndStoreMyDids(JSON.stringify(configJson), JSON.stringify(credentialsJson), JSON.stringify(didJson))
    //         console.log(createPairwiseDidResponse);
    //         const connection: any = await this.createConnection(createPairwiseDidResponse[0], createPairwiseDidResponse[1]);
    //         const invitationDetails: any = await this.createInvitationDetails(connection);
    //         console.log("InvitationDetails", invitationDetails);
    //         const invitation: any = await createInvitationMessage(invitationDetails);
    //         connection.state = ConnectionState.INVITED;
    //         connection.invitation = invitation;
    //         let invitationEncode: any = encodeInvitationToUrl(connection);
    //         console.warn(JSON.stringify(connection));
    //         return invitationEncode;
    //     }
    //     catch (error) {
    //         console.log(error);
    //         return error;
    //     }
    // }

    // async createConnection(did: any, verkey: any): Promise<Connection> {
    //     const did_doc = {
    //         '@context': 'https://w3id.org/did/v1',
    //         service: [
    //             {
    //                 id: 'did:example:123456789abcdefghi#did-communication',
    //                 type: 'did-communication',
    //                 priority: 0,
    //                 recipientKeys: [verkey],
    //                 routingKeys: [],
    //                 serviceEndpoint: DBServices.getServiceEndpoint(),
    //             },
    //         ],
    //     };

    //     const connection = {
    //         did,
    //         didDoc: did_doc,
    //         verkey,
    //         state: ConnectionState.INIT,
    //         messages: [],
    //     };

    //     // this.connections.push(connection);

    //     return connection;
    // }

    // async createInvitationDetails(connection: Connection) {
    //     const { didDoc } = connection;
    //     return {
    //         label: "Temp",
    //         recipientKeys: didDoc.service[0].recipientKeys,
    //         serviceEndpoint: didDoc.service[0].serviceEndpoint,
    //         routingKeys: didDoc.service[0].routingKeys,
    //     };
    // }
}

export default new ConnectionService();
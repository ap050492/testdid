type Did = string;
type Verkey = string;

export interface InvitationDetails {
  label: string;
  recipientKeys: Verkey[];
  serviceEndpoint: string;
  routingKeys: Verkey[];
}

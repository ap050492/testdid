import RouterRequest from "../../RouterRequests";
import { NativeModules } from "react-native";
import { WalletConfig, WalletCredentials } from "DidaasMobileAgent/src/Wallet/WalletInterface";
import { userDetails } from "DidaasMobileAgent/src/Storage";

console.log(NativeModules);
const { DidaasMobileAgent }: any = NativeModules;

class ConnectionService {

  walletDatabase: any;

  constructor() {
    this.walletDatabase = userDetails.objects('wallet')[0];
  }

  // Network Request for Receiving connection object from Routing Agent
  async agentregistration(url: string, apiType: string, apiBody: string, configJson: WalletConfig, credentialsJson: WalletCredentials): Promise<any> {
    try {
      let incomingRouterResponse: any = await RouterRequest(url, apiType, apiBody);
      userDetails.write(() => {
        this.walletDatabase.serviceEndpoint = incomingRouterResponse.responseEndpoint+"/"+incomingRouterResponse.consumer
      });
      let addRecordResponse: any = await DidaasMobileAgent.addWalletRecord(
        JSON.stringify(configJson),
        JSON.stringify(credentialsJson),
        "cloud_agent",
        incomingRouterResponse['@id'],
        JSON.stringify(incomingRouterResponse),
      );
      if(addRecordResponse === null) return Promise.resolve("Agent Registration Successful");
      else return Promise.reject("Agent Registration Failed");
    }
    catch(error) {
      return Promise.reject(error);
    }
  }

}

export default new ConnectionService();

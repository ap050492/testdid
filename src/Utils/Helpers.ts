import { InvitationDetails } from '../Protocols/Connections/ConnectionInterface';
import { DBServices } from '../Storage';
import { Buffer } from 'buffer';

export function decodeInvitationFromUrl(invitationUrl: string) {
    const [, encodedInvitation] = invitationUrl.split('c_i=');
    const invitation = JSON.parse(Buffer.from(encodedInvitation, 'base64').toString());
    return invitation;
}

export function encodeInvitationToUrl(invitation: InvitationDetails): string {
    const encodedInvitation = Buffer.from(JSON.stringify(invitation)).toString('base64');
    let sdkDB: any = DBServices.getWallet();
    const invitationUrl = `${sdkDB.serviceEndpoint.split("/")[0]+"/"+sdkDB.serviceEndpoint.split("/")[1]+"/"+sdkDB.serviceEndpoint.split("/")[2]}/ssi?c_i=${encodedInvitation}`;
    return invitationUrl;
}

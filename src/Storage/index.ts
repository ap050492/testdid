import Realm from 'realm';

const userDetails = new Realm({
    schemaVersion: 1,
    path: 'sdkRealm.realm',
    schema: [
        {
            name: 'wallet',
            properties: {
                walletConfig:'string',
                walletCredentials: 'string',
                lable: 'string',
                publicDid: 'string',
                serviceEndpoint: 'string',
                verKey: 'string'
            },
        },
    ],
});

const DBServices = {
    storeWallet(data) {
        console.warn(data);
        userDetails.write(() => {
            userDetails.create(
                'wallet',
                data,
                true,
            );
        });
    },

    getWallet() {
        const query = userDetails.objects('wallet');
        const array = Array.from(query);
        return array[0];
    },

    getServiceEndpoint () {
        const query = userDetails.objects('wallet');
        const array: any = Array.from(query);
        return array[0].serviceEndpoint;
    },

    getLable () {
        const query = userDetails.objects('wallet');
        const array: any = Array.from(query);
        return array[0].lable;
    },
};

export {
    DBServices,
    userDetails
};